!! Transparency (0-1):
st.alpha: 0.92

!! Set a default font and font size as below:
st.font: Monospace-11;

! st.termname: st-256color
! st.borderpx: 2

!! Set the background, foreground and cursor colors as below:

! special
*.foreground:   #c5c8c6
*.background:   #1d1f21
*.cursorColor:  #c5c8c6

! black
*.color0:       #ffffff
*.color8:       #291b19

! red
*.color1:       #a54242
*.color9:       #cc6666

! green
*.color2:       #8c9440
*.color10:      #b5bd68

! yellow
*.color3:       #de935f
*.color11:      #f0c674

! blue
*.color4:       #5f819d
*.color12:      #81a2be

! magenta
*.color5:       #85678f
*.color13:      #b294bb

! cyan
*.color6:       #5e8d87
*.color14:      #8abeb7

! white
*.color7:       #707880
*.color15:      #c5c8c6


!! URxvt Appearance
URxvt.font: -*-rissole-*
URxvt.boldFont: -*-rissole-* 
URxvt.italicFont: -*-rissole-* 
URxvt.boldItalicfont: -*-rissole-* 
URxvt.letterSpace: 0
URxvt.lineSpace: 0
URxvt.geometry: 92x24
URxvt.internalBorder: 24
URxvt.cursorBlink: true
URxvt.cursorUnderline: false
URxvt.saveline: 2048
URxvt.scrollBar: false
URxvt.scrollBar_right: false
URxvt.urgentOnBell: false
URxvt.depth: 24
URxvt.iso14755: false

!! Copy Paste & Other Extensions
URxvt.perl-ext-common: default,clipboard,url-select,keyboard-select
URxvt.copyCommand: xclip -i -selection clipboard
URxvt.pasteCommand: xclip -o -selection clipboard
URxvt.keysym.M-c: perl:clipboard:copy
URxvt.keysym.M-v: perl:clipboard:paste
URxvt.keysym.M-C-v: perl:clipboard:paste_escaped
URxvt.keysym.M-Escape: perl:keyboard-select:activate
URxvt.keysym.M-s: perl:keyboard-select:search
URxvt.keysym.M-u: perl:url-select:select_next
URxvt.urlLauncher: firefox
URxvt.underlineURLs: true
URxvt.urlButton: 1


!! Common Keybinds for Navigations
URxvt.keysym.Shift-Up: command:\033]720;1\007
URxvt.keysym.Shift-Down: command:\033]721;1\007
URxvt.keysym.Control-Up: \033[1;5A
URxvt.keysym.Control-Down: \033[1;5B
URxvt.keysym.Control-Right: \033[1;5C
URxvt.keysym.Control-Left: \033[1;5D

URxvt*perl-ext-common:
URxvt*perl-ext:

 ! man -Pcat urxvt | sed -n '/th: b/,/^B/p'|sed '$d'|sed '/^ \{7\}[a-z]/s/^ */^/g' | sed -e :a -e 'N;s/\n/@@/g;ta;P;D' | sed 's,\^\([^@]\+\)@*[\t ]*\([^\^]\+\),! \2\n! URxvt*\1\n\n,g' | sed 's,@@\(  \+\),\n\1,g' | sed 's,@*$,,g' | sed '/^[^!]/d' | tr -d "'\`"


! Compile xft: Attempt to find a visual with the given bit depth; option -depth.
! URxvt*depth: bitdepth

! Compile xft: Turn on/off double-buffering for xft (default enabled).  On some card/driver combination enabling it slightly decreases performance, on most it greatly helps it. The slowdown is small, so it should normally be enabled.
! URxvt*buffered: boolean



!! Xterm

xterm*font: hack
xterm*boldFont: hack
xterm*loginShell: true
xterm*vt100*geometry: 80x25
xterm*saveLines: 2000
xterm*charClass: 33:48,35:48,37:48,43:48,45-47:48,64:48,95:48,126:48
xterm*termName: xterm-color
xterm*eightBitInput: false
xterm*boldMode: false
xterm*colorBDMode: true
xterm*colorBD: rgb:fc/fc/fc
xterm*selectToClipboard: true

! special
xterm*.foreground:   #c1c8c5
xterm*.background:   #101111
xterm*.cursorColor:  #c1c8c5

! black
xterm*.color0:       #1a1c1b
xterm*.color8:       #bb94f0

! red
xterm*.color1:       #232524
xterm*.color9:       #efa6c7

! green
xterm*.color2:       #333635
xterm*.color10:      #df7bac

! yellow
xterm*.color3:       #4a4d4c
xterm*.color11:      #a7c068

! blue
xterm*.color4:       #666a69
xterm*.color12:      #faae6c

! magenta
xterm*.color5:       #888d8b
xterm*.color13:      #f4a4c8

! cyan
xterm*.color6:       #afb6b4
xterm*.color14:      #d1bef0

! white
xterm*.color7:       #dce4e1
xterm*.color15:      #71c1dc

